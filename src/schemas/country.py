from typing import Optional

from pydantic import BaseModel


class CountryBase(BaseModel):
    title: Optional[str] = None
    iso_code: Optional[str] = None


# Shared properties
class CountryCreate(CountryBase):
    pass


class CountryUpdate(BaseModel):
    pass
