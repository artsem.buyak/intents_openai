from http import HTTPStatus
from unittest.mock import patch

import pytest


class TestCompletion:
    @pytest.mark.parametrize(
        "reverse_url",
        [
            "text-completion-retrieve",
            "text-intent-retrieve",
        ],
    )
    async def test_completion_retrieve__fail__authenticated(self, app, async_client, user, reverse_url):
        """
        Test authenticated request to make text-completion
        """
        url = app.url_path_for(reverse_url)

        completion_response = await async_client.get(url)

        completion_info = completion_response.json()
        assert completion_response.status_code == HTTPStatus.UNAUTHORIZED
        assert completion_info == {"detail": "Not authenticated"}

    @pytest.mark.parametrize(
        "reverse_url",
        [
            "text-completion-retrieve",
            "text-intent-retrieve",
        ],
    )
    async def test_completion_retrieve__fail__unprocessable_entity(self, app, async_client, user, reverse_url):
        """
        Test authenticated request to make text-completion
        """
        url = app.url_path_for(reverse_url)

        completion_response = await async_client.get(
            url,
            headers={"Authorization": f"Bearer {user.generate_token()}"},
        )

        assert completion_response.status_code == HTTPStatus.UNPROCESSABLE_ENTITY

    @patch(
        "clients.openai.OpenaiClient.get_completion_text",
        return_value="Some text",
    )
    async def test_completion_retrieve__ok__(self, get_completion_text_mock, app, async_client, user):
        """
        Test authenticated request to make text-completion
        """
        q = "Welcome to Canada"
        url = app.url_path_for("text-completion-retrieve")

        completion_response = await async_client.get(
            url,
            headers={"Authorization": f"Bearer {user.generate_token()}"},
            params={
                "q": q,
            },
        )

        completion_info = completion_response.json()
        assert completion_response.status_code == HTTPStatus.OK
        assert completion_info["text"] == "Some text"
        get_completion_text_mock.assert_called_once()

    @patch(
        "clients.openai.OpenaiClient.get_intent",
        return_value={"intent": "Move to Canada"},
    )
    async def test_intent_retrieve__ok__(self, get_completion_text_mock, app, async_client, user):
        """
        Test authenticated request to make text-completion
        """
        q = "Welcome to Canada"
        url = app.url_path_for("text-intent-retrieve")

        completion_response = await async_client.get(
            url,
            headers={"Authorization": f"Bearer {user.generate_token()}"},
            params={
                "q": q,
            },
        )

        completion_info = completion_response.json()
        assert completion_response.status_code == HTTPStatus.OK
        assert completion_info["intent"] == "Move to Canada"
        get_completion_text_mock.assert_called_once()
