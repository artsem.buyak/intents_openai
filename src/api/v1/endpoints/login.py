from datetime import timedelta
from http import HTTPStatus
from typing import Any

from fastapi import Body, Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer
from fastapi_utils.inferring_router import InferringRouter
from pydantic.networks import EmailStr
from sqlalchemy.ext.asyncio import AsyncSession

import schemas
from api import deps
from core import security
from core.config import settings
from db import crud, models

router = InferringRouter()
reusable_oauth2 = OAuth2PasswordBearer(tokenUrl=f"{settings.API_V1_STR}/login/access-token")


@router.post(
    "/login/access-token",
    response_model=schemas.Token,
    name="auth-login",
)
async def login_access_token(
    db: AsyncSession = Depends(deps.get_session),
    *,
    password: str = Body(...),
    email: EmailStr = Body(...),
) -> Any:
    """
    OAuth2 compatible token login, get an access token for future requests
    """
    user = await crud.user.authenticate(
        db,
        email=email,
        password=password,
    )
    if not user:
        raise HTTPException(status_code=HTTPStatus.BAD_REQUEST, detail="Incorrect email or password")
    elif not crud.user.is_active(user):
        raise HTTPException(status_code=HTTPStatus.BAD_REQUEST, detail="Inactive user")
    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    return {
        "access_token": security.create_access_token(user.id, expires_delta=access_token_expires),
        "token_type": "bearer",
    }


@router.post(
    "/logout",
    response_model=schemas.Msg,
    name="auth-logout",
)
async def logout(
    db: AsyncSession = Depends(deps.get_session),
    token: str = Depends(reusable_oauth2),
) -> Any:
    """
    OAuth2 compatible token blacklisted.
    """
    db_obj = models.BlacklistToken(
        token=token,
    )
    db.add(db_obj)
    await db.commit()

    return {"msg": "User logout, token blacklisted."}


@router.post(
    "/login/test-token",
    response_model=schemas.User,
    name="auth-test-token",
)
def test_token(current_user: models.User = Depends(deps.get_current_user)) -> Any:
    """
    Test access token
    """
    return current_user
