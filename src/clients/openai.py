import json
from typing import Dict

import openai
from aiohttp import ClientSession
from openai.openai_object import OpenAIObject

from core.config import settings


class OpenaiClient:
    def __init__(self):
        openai.api_key = settings.OPENAI_API_KEY
        self.intent_prompt = 'Predict intent by phrase. Return in format: {"intent": "value"}\nQuery:'

    async def get_completion_text(self, prompt: str) -> OpenAIObject:
        async with ClientSession() as session:
            openai.aiosession.set(session)
            response: OpenAIObject = await openai.Completion.acreate(
                prompt=prompt,
                engine="text-davinci-003",
                max_tokens=512,
                temperature=0.8,
                frequency_penalty=0.2,
                presence_penalty=0.2,
                top_p=1,
                user="test",
                stream=False,
                n=1,
            )

            return response["choices"][0]["text"].strip()

    async def get_intent(self, input_text: str) -> Dict[str, str]:
        response: OpenAIObject = await self.get_completion_text(
            prompt=f"{self.intent_prompt} {input_text}",
        )

        return json.loads(response["choices"][0]["text"].strip())


openai_client = OpenaiClient()
