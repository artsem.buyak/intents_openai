from http import HTTPStatus
from typing import Any

from fastapi import Depends, Query
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter

import schemas
from api import deps
from clients.openai import openai_client
from db import models

router = InferringRouter()
PREFIX = "/completion"


@cbv(router)
class User:
    current_user: models.User = Depends(deps.get_current_active_user)

    @router.get(
        f"{PREFIX}",
        response_model=schemas.CompletionText,
        status_code=HTTPStatus.OK,
        name="text-completion-retrieve",
    )
    async def get_completion_text(
        self,
        *,
        q: str = Query(...),
    ) -> Any:
        """
        Get answer completion by input text.
        """
        return {
            "text": await openai_client.get_completion_text(
                prompt=q,
            )
        }

    @router.get(
        f"{PREFIX}/intent",
        response_model=schemas.CompletionIntent,
        status_code=HTTPStatus.OK,
        name="text-intent-retrieve",
    )
    async def get_intent(
        self,
        *,
        q: str = Query(...),
    ) -> Any:
        """
        Get intent by input text.
        """
        return await openai_client.get_intent(
            input_text=q,
        )
