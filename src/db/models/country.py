from sqlalchemy import Column, Integer, String

from db.models.base import Base


class Country(Base):
    """
    Shop Countries for storing user related details
    """

    __tablename__ = "countries"  # type: ignore

    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    title = Column(String(100))
    iso_code = Column(String(2), unique=True)

    def __repr__(self):
        return self.title
