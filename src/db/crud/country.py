from typing import Any, Dict, Optional

from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from db.crud.base import CRUDBase
from db.models import Country
from schemas import CountryCreate, CountryUpdate


class CRUDCountry(CRUDBase[Country, CountryCreate, CountryUpdate]):
    async def get_by_iso_code(self, db: AsyncSession, *, iso_code: str) -> Optional[Country]:
        result = await db.execute(select(Country).where(Country.iso_code == iso_code))
        return result.scalar()

    async def get_id_mapping(self, db: AsyncSession) -> Dict[int, str]:
        result = await db.execute(select(self.model.id, self.model.title))
        return dict(result.fetchall())

    async def update(self, *args, **kwargs) -> None:  # type: ignore
        raise NotImplementedError

    async def create(self, *args: Any, **kwargs: Any) -> None:  # type: ignore
        raise NotImplementedError


country = CRUDCountry(Country)
