from datetime import datetime

from fastapi import HTTPException, status
from sqlalchemy import Column, DateTime, Integer, String
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from db.models.base import Base


class BlacklistToken(Base):
    """
    Token Model for storing JWT tokens
    """

    __tablename__ = "blacklist_tokens"  # type: ignore

    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    token = Column(String(500), unique=True, nullable=False, index=True)
    blacklisted_on = Column(DateTime, nullable=False)

    def __init__(self, token):
        self.token = token
        self.blacklisted_on = datetime.now()

    def __repr__(self):
        return "<id: token: {}".format(self.token)

    @staticmethod
    async def check_blacklist(db: AsyncSession, *, auth_token: str):
        # check whether auth token has been blacklisted

        result = await db.execute(select(BlacklistToken).where(BlacklistToken.token == auth_token))
        obj = result.scalar()

        if obj:
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail="Token blacklisted. Please log in again.",
            )
