# FastAPI openai intents


Service for making requests to openai and getting intents by phrases.

There is a logic for adding new users (registration / login) + registration through `apple`.
You can add an icon for the user (the picture itself will be saved on `s3`, in DB only path with bucket name and schema,
use the `localstack` local development).


## Before installation

This projects uses the following standard tools:

- [`fastapi`](https://fastapi.tiangolo.com) - FastAPI framework, high performance, easy to learn, fast to code, ready for production;
- [`async sqlalchemy`](https://docs.sqlalchemy.org/en/14/orm/extensions/asyncio.html) - async version of Sqlalchemy ORM;
- [`poetry`](https://python-poetry.org/docs) — to manage Python project (install dependencies, run tests and linters);
- [`docker`](https://www.docker.com) — to run external services like databases, caches, etc;
- [`pytest`](https://docs.pytest.org) - The pytest framework makes it easy to write small, readable tests, and can scale to
      support complex functional testing for applications and libraries.
- [`factory_boy`](https://factoryboy.readthedocs.io) - As a fixtures replacement tool, it aims to replace static, 
      hard to maintain fixtures with easy-to-use factories for complex objects.

## Installation

Install Poetry and Python dependencies:

.env-base it's example of your own local .env file. Write it with your vars and put into path env/.env

```shell
$ make install
```

## Configuration and launch

Run migrations:

```shell
$ make migrate
```

Run application:

```shell
$ make service
```

## Updating and development

To update application dependencies, run:

```shell
$ make update
```

It will produce your `poetry.lock`, with all dependencies (and all underlying dependencies) pinned.

To create new migration from your DB model run:

```shell
$ make migrations
```

## Testing and checking code style

For tests it's enough to run:

```shell
$ make tests
```

Before submitting a PR, please also run:

```shell
$ make lint
```

Note, that for all repositories in GitLab code style check is enforced, so any PR will be blocked until style issues aren't resolved.

## Docker image

Docker image is used for both - running in production and on developers' machines as dependency (use `docker-compose`). Here's how you can build docker image locally.

If you have made any changes to `Dockerfile`, first list it:

```shell
$ make docker-lint
```

Then run the build:

```shell
$ make docker-build
or 
$ make docker-build-m1 # In case of using apple silicon
```

## Api description path

```shell
/docs
```