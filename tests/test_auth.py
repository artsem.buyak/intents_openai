from datetime import datetime as dt
from http import HTTPStatus


class TestUserRegistration:
    async def test_register_user__signup__ok__(self, app, async_client):
        """
        Test for sighup new User
        """

        email = f"bidon+{dt.now().strftime('%m%d%Y%H%M%S%f')}@pomoev.com"
        full_name = "Artsem Test"
        password = "some_password"

        url = app.url_path_for("users-create")

        # user signup
        user_response = await async_client.post(
            url=url,
            json=dict(
                email=email,
                full_name=full_name,
                password=password,
            ),
        )
        response_data = user_response.json()
        assert user_response.status_code == HTTPStatus.CREATED

        assert response_data["email"] == email
        assert response_data["is_active"] is True
        assert response_data["is_superuser"] is False
        assert response_data["full_name"] == full_name

    async def test_register_user__signup__fail__unexpected_entity(self, app, async_client):
        """
        Test signup with wrong email
        """
        email = "wrong_email"
        full_name = "Artsem Test"
        password = "some_password"

        url = app.url_path_for("users-create")

        # user signup
        user_response = await async_client.post(
            url=url,
            json=dict(
                email=email,
                full_name=full_name,
                password=password,
            ),
        )
        response_data = user_response.json()

        assert user_response.status_code == HTTPStatus.UNPROCESSABLE_ENTITY
        assert response_data["detail"][0]["msg"] == "value is not a valid email address"

    async def test_register_user__signup__fail__required_fields(self, app, async_client):
        """
        Test signup without password
        """
        email = "wrong_email"
        full_name = "Artsem Test"

        url = app.url_path_for("users-create")

        # user signup
        user_response = await async_client.post(
            url=url,
            json=dict(
                email=email,
                full_name=full_name,
                password=None,
            ),
        )
        response_data = user_response.json()

        assert user_response.status_code == HTTPStatus.UNPROCESSABLE_ENTITY
        assert response_data["detail"][0]["msg"] == "field required"

    async def test_register_user__login__ok__(self, app, async_client):
        """
        Test success login
        """

        email = f"bidon+{dt.now().strftime('%m%d%Y%H%M%S%f')}@pomoev.com"
        full_name = "Artsem Test"
        password = "some_password"

        url = app.url_path_for("users-create")

        # user signup
        user_response = await async_client.post(
            url=url,
            json=dict(
                email=email,
                full_name=full_name,
                password=password,
            ),
        )

        response_data = user_response.json()
        assert user_response.status_code == HTTPStatus.CREATED

        url = app.url_path_for("auth-login")

        # user login
        login_response = await async_client.post(
            url=url,
            json=dict(
                email=response_data["email"],
                password=password,
            ),
        )

        login_info = login_response.json()
        assert login_response.status_code == HTTPStatus.OK
        assert login_info["token_type"] == "bearer"
        assert isinstance(login_info["access_token"], str)

    async def test_register_user__login__fail__unregistered(self, app, async_client):
        """
        Test login for unregistered user
        """
        url = app.url_path_for("auth-login")
        email = f"bidon+{dt.now().strftime('%m%d%Y%H%M%S%f')}@test.com"
        password = "without registration"

        # user login
        response = await async_client.post(
            url=url,
            json=dict(
                email=email,
                password=password,
            ),
        )

        login_info = response.json()
        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert login_info["detail"] == "Incorrect email or password"

    async def test_register_user__logout__ok__(self, app, async_client, user):
        """
        Test success logout
        """

        url = app.url_path_for("auth-logout")

        response = await async_client.post(
            url=url,
            headers={"Authorization": f"Bearer {user.generate_token()}"},
        )

        logout_info = response.json()
        assert response.status_code == HTTPStatus.OK
        assert logout_info["msg"] == "User logout, token blacklisted."

    async def test_register_user__delete__ok__(self, app, async_client, user):
        """
        Test delete user
        """

        url = app.url_path_for("users-delete")

        response = await async_client.delete(url=url, headers={"Authorization": f"Bearer {user.generate_token()}"})

        assert response.status_code == HTTPStatus.NO_CONTENT
