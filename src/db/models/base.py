from io import BytesIO
from typing import Any

from sqlalchemy import Column, String
from sqlalchemy.ext.declarative import as_declarative, declared_attr

from clients.s3 import s3_client


@as_declarative()
class Base:
    id: Any
    __name__: str

    @declared_attr
    def __tablename__(cls) -> str:
        return cls.__name__.lower()


class ImageFieldMixin:
    BASE_IMAGE_PATH = "database/images/{table_name}/{user_id}/{scan_id}/{filename}"

    # path to s3 folder without s3://BUCKET_NAME prefix, aka /database/some_path/image.jpg
    image_path = Column(String(1500), nullable=True)

    @property
    async def image(self) -> bytes:
        return await s3_client.download_file(self.image_path)

    async def remove_image(self) -> None:
        return await s3_client.remove_file(self.image_path)

    async def upload_image(self, file: BytesIO):
        await s3_client.upload_file(
            file_path=self.image_path,
            file_obj=file,
        )
