import enum
import random
from datetime import timedelta

from sqlalchemy import (
    Boolean,
    Column,
    Date,
    DateTime,
    ForeignKey,
    Index,
    Integer,
    String,
    UniqueConstraint,
)
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from core.config import settings
from core.security import create_access_token, get_password_hash
from db.models.base import Base, ImageFieldMixin
from db.models.country import Country


class SocialAuthTypes(str, enum.Enum):
    APPLE = "apple"


class User(Base, ImageFieldMixin):
    """
    User Model for storing user related details
    """

    BASE_IMAGE_PATH = "database/images/{table_name}/{user_id}/{filename}"

    __tablename__ = "users"  # type: ignore

    id = Column(Integer, primary_key=True, autoincrement=True, index=True)

    email = Column(String(255), unique=True, nullable=False)
    created_at = Column(DateTime, nullable=False, default=func.now())
    is_active = Column(Boolean(), default=True)
    is_superuser = Column(Boolean(), default=False)
    admin = Column(Boolean, nullable=False, default=False)
    public_id = Column(String(100), unique=True)
    username = Column(String(50), unique=True)
    password_hash = Column(String(100))
    full_name = Column(String(100), nullable=True)
    is_temp = Column(Boolean(), default=False)
    date_of_birth = Column(Date, nullable=True, default=None)

    # Foreign keys
    country_id = Column(Integer, ForeignKey(Country.id), nullable=True, index=True)
    country = relationship(Country)

    def __repr__(self):
        return self.email

    @property
    def password(self):
        return self.password_hash

    @password.setter
    def password(self, password):
        self.password_hash = get_password_hash(password)

    @staticmethod
    def get_random_string(length=12, allowed_chars="abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789") -> str:
        """
        Return a securely generated random string.
        """

        return "".join(random.choice(allowed_chars) for _ in range(length))

    @staticmethod
    def make_random_password(
        length=12,
        allowed_chars="abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789",
    ):
        while True:
            password = User.get_random_string(length=length, allowed_chars=allowed_chars)

            if any(char.isalpha() for char in password) and any(char.isdigit() for char in password):
                break

        return password

    @staticmethod
    def get_random_login():
        return "{prefix}_{hash}@{domain}".format(
            **{
                "prefix": "temp",
                "hash": User.get_random_string().lower(),
                "domain": settings.EMAIL_DEFAULT_DOMAIN,
            }
        )

    def generate_token(self) -> str:
        return create_access_token(self.id, expires_delta=timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES))


class UserSocial(Base):
    __tablename__ = "users_social"  # type: ignore

    __table_args__ = (
        UniqueConstraint(
            "social_type",
            "social_id",
            name="social_type__social_id__uix_1",
        ),
        Index("social_type__social_id__udx_1", "social_type", "social_id", unique=True),
    )

    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    social_type = Column(String(100), default=SocialAuthTypes.APPLE.value)

    # sub
    # The subject registered claim identifies the principal that’s the subject of the identity token.
    # Since this token is meant for your application, the value is the unique identifier for the user.
    social_id = Column(String(64))

    # Foreign keys
    user_id = Column(
        Integer,
        ForeignKey(User.id, ondelete="CASCADE"),
    )
    user = relationship(User, backref="user_social")

    def __repr__(self):
        return self.social_id
