from typing import Optional

from pydantic import BaseModel


class CompletionText(BaseModel):
    text: Optional[str] = None


class CompletionIntent(BaseModel):
    intent: Optional[str] = None
