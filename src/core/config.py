import os
import secrets
from typing import Any, Dict, List, Optional, Union

from pydantic import AnyHttpUrl, BaseSettings, PostgresDsn, validator


class Settings(BaseSettings):
    TEST_MODE: bool = False
    AWS_REQUEST_PAYER: str = "requester"
    AWS_ACCESS_KEY_ID: str = ""
    AWS_SECRET_ACCESS_KEY: str = ""
    AWS_S3_ENDPOINT_URL: Optional[str] = None
    AWS_S3_BUCKET_NAME: str = "intents-openai-data"

    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    API_V1_STR: str = "/api/v1"
    SECRET_KEY: str = secrets.token_urlsafe(32)

    # 60 minutes * 24 hours * 8 days = 8 days
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 60 * 24 * 8
    USERS_OPEN_REGISTRATION: bool = True
    EMAIL_RESET_TOKEN_EXPIRE_HOURS: int = 24
    EMAIL_DEFAULT_DOMAIN: str = "intents.temp"

    BACKEND_CORS_ORIGINS: List[AnyHttpUrl] = []

    POSTGRES_SERVER: str
    POSTGRES_USER: str
    POSTGRES_PASSWORD: str
    POSTGRES_DB: str
    POSTGRES_PORT: int
    SQLALCHEMY_DATABASE_URI: Optional[PostgresDsn] = None

    PROJECT_NAME: str = "src"
    SERVER_HOST: str = "localhost:8000"

    SENTRY_DSN: str = ""
    SENTRY_ENV: str = "local"

    # ************ SOCIAL AUTH ************
    # Sign in with Apple
    # https://developer.apple.com/sign-in-with-apple/get-started/
    APPLE_BASE_URL = "https://appleid.apple.com"
    APPLE_APP_ID = "platform.intents.app.ScoutingApp.debug"
    APPLE_CLIENT_SECRET_ALGORITHM = "ES256"
    APPLE_GRANT_TYPE = "authorization_code"
    APPLE_JWT_ALGORITHM = "RS256"
    APPLE_KEY_ID = "ABCDE12345"
    APPLE_TEAM_ID = "ABCDE12345"
    APPLE_TOKEN_TTL = 180
    CONFIG_FILES_DIR = os.path.join(os.path.dirname(BASE_DIR), "data", "config")

    OPENAI_API_KEY: str = ""

    @validator("BACKEND_CORS_ORIGINS", pre=True)
    def assemble_cors_origins(cls, v: Union[str, List[str]]) -> Union[List[str], str]:
        if isinstance(v, str) and not v.startswith("["):
            return [i.strip() for i in v.split(",")]
        elif isinstance(v, (list, str)):
            return v
        raise ValueError(v)

    @validator("SQLALCHEMY_DATABASE_URI", pre=True)
    def assemble_db_connection(cls, v: Optional[str], values: Dict[str, Any]) -> Any:
        if isinstance(v, str):
            return v
        return PostgresDsn.build(
            scheme="postgresql+asyncpg",
            user=values.get("POSTGRES_USER"),
            password=values.get("POSTGRES_PASSWORD"),
            host=values.get("POSTGRES_SERVER"),
            path=f":{values.get('POSTGRES_PORT')}/{values.get('POSTGRES_DB') or ''}",
        )

    class Config:
        case_sensitive = True
        env_file = "env/.env"


settings = Settings()
