def guess_image_mime_type(data: bytes):
    """
    Function guesses an image mime type.
    Supported filetypes are JPG, BMP, PNG.
    """
    if data[:4] == "\xff\xd8\xff\xe0" and data[6:] == "JFIF\0":
        return "image/jpeg"
    elif data[1:4] == "PNG":
        return "image/png"
    elif data[:2] == "BM":
        return "image/x-ms-bmp"
    else:
        return "image/unknown-type"
