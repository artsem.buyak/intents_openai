from typing import Any, Dict, Optional, Union

from sqlalchemy import and_
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from core.security import create_access_token, get_password_hash, verify_password
from db.crud.base import CRUDBase
from db.models import Country, User, UserSocial
from schemas.user import (
    UserCreate,
    UserSocialCreate,
    UserSocialUpdate,
    UserTempUpdate,
    UserUpdate,
)


class CRUDUser(CRUDBase[User, UserCreate, UserUpdate]):
    async def get_by_email(
        self,
        db: AsyncSession,
        *,
        email: str,
        is_temp: bool = False,
    ) -> Optional[User]:
        result = await db.execute(
            select(User).where(
                and_(
                    User.email == email,
                    User.is_temp.is_(is_temp),
                )
            ),
        )
        return result.scalar()

    async def get_temp_user_by_id(
        self,
        db: AsyncSession,
        *,
        id: int,
    ) -> Optional[User]:
        result = await db.execute(
            select(User).where(
                and_(
                    User.id == id,
                    User.is_temp.is_(True),
                )
            ),
        )
        return result.scalar()

    async def create(self, db: AsyncSession, *, obj_in: UserCreate) -> User:
        db_obj = User(  # type: ignore
            email=obj_in.email,
            password=obj_in.password,
            full_name=obj_in.full_name,
            is_superuser=obj_in.is_superuser,
            is_temp=obj_in.is_temp,
            country_id=obj_in.country_id,
        )
        db.add(db_obj)
        await db.commit()
        await db.refresh(db_obj)

        return db_obj

    async def update(
        self,
        db: AsyncSession,
        *,
        db_obj: User,
        obj_in: Union[UserUpdate, Dict[str, Any], UserTempUpdate],
    ) -> User:
        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.dict(exclude_unset=True)
        if update_data.get("password"):
            password_hash = get_password_hash(update_data["password"])
            del update_data["password"]
            update_data["password_hash"] = password_hash

        return await super().update(db, db_obj=db_obj, obj_in=update_data)

    async def authenticate(self, db: AsyncSession, *, email: str, password: str) -> Optional[User]:
        user = await self.get_by_email(db, email=email)
        if not user:
            return None
        if not verify_password(password, user.password_hash):
            return None
        return user

    def is_active(self, user: User) -> bool:
        return user.is_active

    def is_superuser(self, user: User) -> bool:
        return user.is_superuser

    def is_admin(self, user: User) -> bool:
        return user.admin


user = CRUDUser(User)


class CRUDUserSocial(CRUDBase[UserSocial, UserSocialCreate, UserSocialUpdate]):
    async def get_by_social_id(
        self,
        db: AsyncSession,
        *,
        social_id: str,
        social_type: str,
    ) -> Optional[UserSocial]:
        result = await db.execute(
            select(UserSocial).where(
                and_(
                    UserSocial.social_id == social_id,
                    UserSocial.social_type == social_type,
                )
            )
        )
        return result.scalar()

    async def get_by_user_id(
        self,
        db: AsyncSession,
        *,
        user_id: int,
    ) -> Optional[UserSocial]:
        result = await db.execute(select(UserSocial).where(UserSocial.user_id == user_id))
        return result.scalar()

    async def signup(
        self,
        db: AsyncSession,
        *,
        social_type: str,
        social_id: str,
        email: str,
        temp_user: Optional[User] = None,
        country: Optional[Country] = None,
    ) -> Optional[tuple[User, str]]:
        if temp_user:
            user_in = UserTempUpdate(
                email=email,
                is_temp=False,
            )

            user_obj = await user.update(db, db_obj=temp_user, obj_in=user_in)
        else:
            password = User.make_random_password()

            user_in = UserCreate(
                password=password,
                email=email,
                country_id=country.id if country else None,
            )
            user_obj = await user.create(db, obj_in=user_in)

        user_social = UserSocialCreate(
            user_id=user_obj.id,
            social_type=social_type,
            social_id=social_id,
        )
        await self.create(db=db, obj_in=user_social)

        token = create_access_token(subject=user_obj.id)

        return user_obj, token


user_social = CRUDUserSocial(UserSocial)
