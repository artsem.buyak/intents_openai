import json
import os
from datetime import datetime, timedelta
from http import HTTPStatus
from typing import Any, Optional

import jwt
import requests_async as requests
from fastapi import Body, Depends, HTTPException, Path, Request, status
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from jwt.algorithms import RSAAlgorithm
from pydantic import ValidationError
from sqlalchemy.ext.asyncio import AsyncSession

import schemas
from api import deps
from core import security
from core.config import settings
from db import crud, models
from db.utils import get_country_by_ip, get_real_ip

router = InferringRouter()


@cbv(router)
class SocialAuth:
    apple_private_key = None
    session: AsyncSession = Depends(deps.get_session)

    def _get_apple_client_secret(self, client_id):
        headers = {
            "alg": settings.APPLE_CLIENT_SECRET_ALGORITHM,
            "kid": settings.APPLE_KEY_ID,
        }

        now = datetime.now()

        payload = {
            "iss": settings.APPLE_TEAM_ID,
            "iat": now,
            "exp": now + timedelta(days=settings.APPLE_TOKEN_TTL),
            "aud": settings.APPLE_BASE_URL,
            "sub": client_id,
        }

        return jwt.encode(
            payload=payload,
            key=SocialAuth._get_apple_private_key(),
            algorithm=settings.APPLE_CLIENT_SECRET_ALGORITHM,
            headers=headers,
        ).decode("utf-8")

    @classmethod
    def _get_apple_private_key(cls):
        if cls.apple_private_key is not None:
            return cls.apple_private_key

        private_key_path = os.path.join(settings.CONFIG_FILES_DIR, f"AuthKey_{settings.APPLE_KEY_ID}.p8")

        with open(private_key_path) as private_key_file:
            cls.apple_private_key = private_key_file.read()

        return cls.apple_private_key

    @staticmethod
    async def _get_apple_jwk(kid=None):
        response = await requests.get(f"{settings.APPLE_BASE_URL}/auth/keys")

        if not response.ok:
            return None

        data = response.json()
        keys = data.get("keys")

        if not isinstance(keys, list) or not keys:
            return None

        if kid:
            return json.dumps(next(iter([key for key in keys if key["kid"] == kid]), {}))
        else:
            return [json.dumps(key) for key in keys]

    async def _verify_apple_code(self, code, client_id):
        data = {
            "client_id": client_id,
            "client_secret": self._get_apple_client_secret(client_id),
            "code": code,
            "grant_type": settings.APPLE_GRANT_TYPE,
        }

        response = await requests.post(
            f"{settings.APPLE_BASE_URL}/auth/token",
            data=data,
        )

        if not response.ok:
            return None

        try:
            token = response.json()["id_token"]
        except (ValueError, KeyError):
            return None

        return await self.decode_apple_token(token, client_id)

    async def decode_apple_token(self, token, client_id):
        try:
            id_info = jwt.decode(
                jwt=token,
                key=RSAAlgorithm.from_jwk(
                    await SocialAuth._get_apple_jwk(jwt.get_unverified_header(token).get("kid"))
                ),
                audience=client_id,
                algorithm=settings.APPLE_JWT_ALGORITHM,
            )
        except (ValueError, jwt.PyJWTError):
            id_info = None

        return id_info

    async def get_user_by_jwt(self, token):
        try:
            payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[security.ALGORITHM])
            token_data = schemas.TokenPayload(**payload)
        except (jwt.PyJWTError, ValidationError):
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail="Could not validate credentials",
            )
        user = await crud.user.get_temp_user_by_id(self.session, id=token_data.sub)
        if not user:
            raise HTTPException(status_code=404, detail="User not found")

        return user

    async def fetch_apple_user(self, code, client_id):
        id_info = await self._verify_apple_code(code, client_id)

        if id_info is None or id_info["iss"] != settings.APPLE_BASE_URL:
            return None, None, {}

        return id_info["sub"], id_info["email"], {}

    @router.post(
        "/signup/{social_type}",
        response_model=schemas.UserTokenPayload,
        status_code=HTTPStatus.OK,
    )
    async def social_signup(
        self,
        *,
        token: str = Body(None),
        code: str = Body(None),
        temp_user_token: str = Body(None),
        social_type: str = Path("apple"),
        request: Request,
    ) -> Any:
        if social_type == models.SocialAuthTypes.APPLE:
            client_id = settings.APPLE_APP_ID

            social_id, email, social_data = await self.fetch_apple_user(code, client_id)
        else:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Social type not implemented.",
            )

        if social_id is None:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="No social id.",
            )

        if email is None:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="No email.",
            )
        user = await crud.user.get_by_email(self.session, email=email)
        if user:
            social_user = await crud.user_social.get_by_user_id(db=self.session, user_id=user.id)
            if not social_user:
                user_social = schemas.UserSocialCreate(
                    user_id=user.id,
                    social_type=social_type,
                    social_id=social_id,
                )
                await crud.user_social.create(db=self.session, obj_in=user_social)

            access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
            return schemas.UserTokenPayload(
                access_token=security.create_access_token(user.id, expires_delta=access_token_expires),
                token_type="bearer",
                user_id=user.id,
                email=user.email,
                created=False,
                created_at=user.created_at,
            )

        temp_user = None

        if temp_user_token:
            temp_user = await self.get_user_by_jwt(temp_user_token)
            if temp_user_token and temp_user is False:
                raise HTTPException(
                    status_code=status.HTTP_400_BAD_REQUEST,
                    detail="Wrong temp user token",
                )

        country = None
        ip_address: Optional[str] = get_real_ip(request=request)

        if ip_address:
            country = await get_country_by_ip(session=self.session, ip_address=ip_address)

        try:
            user, token = await crud.user_social.signup(
                self.session,
                social_type=social_type,
                social_id=social_id,
                email=email,
                temp_user=temp_user,
                country=country,
            )
        except Exception:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Error during social signup.",
            )

        return schemas.UserTokenPayload(
            access_token=token,
            token_type="bearer",
            user_id=user.id,
            email=user.email,
            created=True,
            created_at=user.created_at,
        )
