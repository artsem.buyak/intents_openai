import asyncio
import inspect
from datetime import datetime

import factory
from factory.alchemy import SQLAlchemyModelFactory

from db.models import User
from db.models.session import SessionLocal


class BaseAsyncFactory(SQLAlchemyModelFactory):
    @staticmethod
    async def create_async(obj):
        async with SessionLocal() as session:
            session.add(obj)
            await session.commit()
            await session.refresh(obj)

            return obj

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        async def maker_coroutine():
            for key, value in kwargs.items():
                # when using SubFactory, you'll have a Task in the corresponding kwarg
                # await tasks to pass model instances instead
                if inspect.isawaitable(value):
                    kwargs[key] = await value

            return await cls.create_async(obj=model_class(*args, **kwargs))

        return asyncio.create_task(maker_coroutine())

    @classmethod
    async def create_batch(cls, size, **kwargs):
        return [await cls.create(**kwargs) for _ in range(size)]


class UserFactory(BaseAsyncFactory):
    email = factory.Sequence(lambda n: f"bidon.dobra+{int(datetime.now().timestamp()) + n}@intents.com")
    password = User.make_random_password()

    class Meta:
        model = User
