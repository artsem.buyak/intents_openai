from http import HTTPStatus
from typing import Any, List

from fastapi import Depends, HTTPException
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from sqlalchemy.ext.asyncio import AsyncSession

import schemas
from api import deps
from db import crud, models

router = InferringRouter()


@cbv(router)
class Users:
    current_user: models.User = Depends(deps.get_current_active_superuser)
    session: AsyncSession = Depends(deps.get_session)

    @router.get("/users", response_model=List[schemas.User], status_code=HTTPStatus.OK)
    async def list(
        self,
        *,
        skip: int = 0,
        limit: int = 100,
    ) -> Any:
        """
        Retrieve users.
        """
        users = await crud.user.get_multi(self.session, skip=skip, limit=limit)
        return users

    @router.post("/users", response_model=schemas.User, status_code=HTTPStatus.CREATED)
    async def create(
        self,
        *,
        user_in: schemas.UserCreate,
    ) -> Any:
        """
        Create new user.
        """
        user = await crud.user.get_by_email(self.session, email=user_in.email)
        if user:
            raise HTTPException(
                status_code=HTTPStatus.BAD_REQUEST,
                detail="The user with this username already exists in the system.",
            )
        user = await crud.user.create(self.session, obj_in=user_in)

        return user

    @router.get("/users/{user_id}", response_model=schemas.User, status_code=HTTPStatus.OK)
    async def retrieve(
        self,
        *,
        user_id: int,
    ) -> Any:
        """
        Get a specific user by id.
        """
        user = await crud.user.get(self.session, id=user_id)

        if not user:
            raise HTTPException(
                status_code=HTTPStatus.NOT_FOUND,
                detail="No user for that id",
            )

        if user == self.current_user:
            return user

        if not crud.user.is_superuser(self.current_user):
            raise HTTPException(
                status_code=HTTPStatus.BAD_REQUEST,
                detail="The user doesn't have enough privileges",
            )

        return user

    @router.patch("/users/{user_id}", response_model=schemas.User, status_code=HTTPStatus.OK)
    async def partial_update(
        self,
        *,
        user_id: int,
        user_in: schemas.UserUpdate,
    ) -> Any:
        """
        Update a user.
        """
        user = await crud.user.get(self.session, id=user_id)
        if not user:
            raise HTTPException(
                status_code=HTTPStatus.NOT_FOUND,
                detail="The user with this username does not exist in the system",
            )
        return await crud.user.update(self.session, db_obj=user, obj_in=user_in)

    @router.delete("/users/{user_id}", response_model=schemas.User, status_code=HTTPStatus.ACCEPTED)
    async def delete(
        self,
        *,
        user_id: int,
    ) -> Any:
        """
        Delete a specific user by id.
        """
        user = await crud.user.remove(self.session, id=user_id)

        return user
