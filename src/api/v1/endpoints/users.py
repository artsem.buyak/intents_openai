from datetime import date, timedelta
from http import HTTPStatus
from typing import Any, Optional

from fastapi import Body, Depends, File, HTTPException, Request, UploadFile
from fastapi.encoders import jsonable_encoder
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from pydantic.networks import EmailStr
from sqlalchemy.ext.asyncio import AsyncSession
from starlette.responses import Response

import schemas
from api import deps
from api.utils import guess_image_mime_type
from clients.s3 import s3_client
from core import security
from core.config import settings
from db import crud, models
from db.utils import get_country_by_ip, get_random_login, get_real_ip

router = InferringRouter()
PREFIX = "/users"


@cbv(router)
class User:
    session: AsyncSession = Depends(deps.get_session)
    current_user: models.User = Depends(deps.get_current_active_user)

    @router.get(
        f"{PREFIX}",
        response_model=schemas.User,
        status_code=HTTPStatus.OK,
        name="users-retrieve",
    )
    async def retrieve(
        self,
    ) -> Any:
        """
        Get current user.
        """
        return self.current_user

    @router.patch(
        f"{PREFIX}",
        response_model=schemas.User,
        status_code=HTTPStatus.OK,
        name="users-partial-update",
    )
    async def partial_update(
        self,
        *,
        full_name: str = Body(None),
        date_of_birth: date = Body(None),
    ) -> Any:
        """
        Update current user.
        """
        current_user_data = jsonable_encoder(self.current_user)
        user_in = schemas.UserUpdate(**current_user_data)
        if full_name is not None:
            user_in.full_name = full_name
        if date_of_birth:
            user_in.date_of_birth = date_of_birth

        return await crud.user.update(self.session, db_obj=self.current_user, obj_in=user_in)

    @router.delete(
        f"{PREFIX}",
        name="users-delete",
    )
    async def delete(
        self,
    ) -> Any:
        """
        Delete current user.
        """
        if self.current_user.image_path:
            await self.current_user.remove_image()

        await crud.user.remove(self.session, id=self.current_user.id)

        return Response(status_code=HTTPStatus.NO_CONTENT)

    @router.get(
        f"{PREFIX}/image",
        status_code=HTTPStatus.NO_CONTENT,
        name="users-image",
    )
    async def image(self):
        """
        Get user avatar image
        """
        if not self.current_user.image_path:
            return Response(status_code=HTTPStatus.NO_CONTENT)

        body = await self.current_user.image

        media_type = guess_image_mime_type(data=body)
        return Response(body, media_type=media_type)

    @router.post(
        f"{PREFIX}/image",
        status_code=HTTPStatus.OK,
        name="users-image",
    )
    async def upload(self, file: UploadFile = File(...)):
        """
        Upload image connected if scan object
        """

        file_path = models.User.BASE_IMAGE_PATH.format(
            table_name=models.User.__tablename__,
            user_id=self.current_user.id,
            filename=file.filename,
        )

        current_user_data = jsonable_encoder(self.current_user)
        user_in = schemas.UserUpdate(**current_user_data)
        user_in.image_path = file_path

        await s3_client.upload_file(
            file_path,
            file_obj=file,
        )

        await crud.user.update(self.session, db_obj=self.current_user, obj_in=user_in)


@router.post(
    f"{PREFIX}",
    response_model=schemas.UserInDB,
    status_code=HTTPStatus.CREATED,
    name="users-create",
)
async def create(
    session: AsyncSession = Depends(deps.get_session),
    *,
    password: str = Body(...),
    email: EmailStr = Body(...),
    full_name: str = Body(None),
) -> Any:
    """
    Create new user without the need to be logged in.
    """
    if not settings.USERS_OPEN_REGISTRATION:
        raise HTTPException(
            status_code=HTTPStatus.FORBIDDEN,
            detail="Open user registration is forbidden on this server",
        )
    user = await crud.user.get_by_email(session, email=email)
    if user:
        raise HTTPException(
            status_code=HTTPStatus.BAD_REQUEST,
            detail="The user with this username already exists in the system",
        )
    user_in = schemas.UserCreate(password=password, email=email, full_name=full_name)
    user = await crud.user.create(session, obj_in=user_in)

    return user


@router.post(
    f"{PREFIX}/signup/temp",
    response_model=schemas.UserTokenPayload,
    status_code=HTTPStatus.CREATED,
    name="users-signup-temp",
)
async def create_temp_user(
    request: Request,
    session: AsyncSession = Depends(deps.get_session),
) -> Any:
    """
    Create new user without the need to be logged in (TEMP USER).
    """
    if not settings.USERS_OPEN_REGISTRATION:
        raise HTTPException(
            status_code=HTTPStatus.FORBIDDEN,
            detail="Open user registration is forbidden on this server",
        )

    country = None
    ip_address: Optional[str] = get_real_ip(request=request)

    if ip_address:
        country = await get_country_by_ip(session=session, ip_address=ip_address)

    user_in = schemas.UserCreate(
        password=models.User.make_random_password(),
        email=get_random_login(),
        is_temp=True,
        country_id=country.id if country else None,
    )
    user = await crud.user.create(session, obj_in=user_in)

    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)

    return schemas.UserTokenPayload(
        access_token=security.create_access_token(user.id, expires_delta=access_token_expires),
        token_type="bearer",
        user_id=user.id,
        email=user.email,
        created=True,
        created_at=user.created_at,
    )
