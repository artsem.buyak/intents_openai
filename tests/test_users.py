from http import HTTPStatus


class TestUserInfo:
    async def test_users__retrieve__ok__(self, app, async_client, user):
        """
        Test retrieve user info
        """
        url = app.url_path_for("users-retrieve")

        user_response = await async_client.get(
            url,
            headers={"Authorization": f"Bearer {user.generate_token()}"},
        )

        user_info = user_response.json()
        assert user_response.status_code == HTTPStatus.OK
        assert user_info["email"] == user.email
        assert user_info["full_name"] == user.full_name

    async def test_users_signup__temp_user__ok__(self, app, async_client):
        """
        Test signup temp user
        """

        url = app.url_path_for("users-signup-temp")
        user_response = await async_client.post(url)

        user_info = user_response.json()
        assert user_response.status_code == HTTPStatus.CREATED
        assert "access_token" in user_info
        assert "token_type" in user_info
        assert user_info["token_type"] == "bearer"
