from datetime import date
from typing import Optional

from pydantic import BaseModel, EmailStr


# Shared properties
class UserBase(BaseModel):
    email: Optional[EmailStr] = None
    is_active: Optional[bool] = True
    is_superuser: bool = False
    full_name: Optional[str] = None
    country_id: Optional[int] = None
    is_temp: Optional[bool] = None
    date_of_birth: Optional[date] = None


# Properties to receive via API on creation
class UserCreate(UserBase):
    email: EmailStr
    password: str
    is_temp: bool = False


class UserSocialCreate(BaseModel):
    user_id: int
    social_type: str
    social_id: str


# Properties to receive via API on update
class UserUpdate(UserBase):
    password: Optional[str] = None
    image_path: Optional[str] = None


class UserTempUpdate(BaseModel):
    email: Optional[EmailStr] = None
    is_temp: Optional[bool] = False


class UserSocialUpdate(BaseModel):
    pass


class UserInDBBase(UserBase):
    id: Optional[int] = None

    class Config:
        orm_mode = True


# Additional properties to return via API
class User(UserInDBBase):
    is_temp: bool = False


class UserResponse(UserInDBBase):
    password: Optional[str] = None


# Additional properties stored in DB
class UserInDB(UserInDBBase):
    password_hash: str
    image_path: Optional[str] = None
