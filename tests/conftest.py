import asyncio

import pytest
from httpx import AsyncClient

from core.main import app as base_app
from db.models.session import SessionLocal
from tests.factories import UserFactory


@pytest.fixture
def app():
    yield base_app


@pytest.fixture(scope="session")
def event_loop():
    policy = asyncio.get_event_loop_policy()
    loop = policy.new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
async def session():
    async with SessionLocal() as session:
        yield session


@pytest.fixture()
async def async_client():
    async with AsyncClient(app=base_app, base_url="http://test") as ac:
        yield ac


@pytest.fixture()
async def user():
    yield await UserFactory()
