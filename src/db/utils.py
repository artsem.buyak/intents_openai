import os
import random
from datetime import datetime
from typing import Optional

import geoip2.database
from fastapi import Request
from geoip2.errors import AddressNotFoundError
from sqlalchemy.ext.asyncio import AsyncSession

from core.config import settings
from db import crud
from db.models.country import Country

geo_ip_db_path = private_key_path = os.path.join(settings.CONFIG_FILES_DIR, "GeoLite2-Country.mmdb")


def get_random_login(
    length=12,
    allowed_chars="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
) -> str:
    """
    Return a securely generated random string.

    The default length of 12 with the a-z, A-Z, 0-9 character set returns
    a 71-bit value. log_2((26+26+10)^12) =~ 71 bits
    """

    return "{prefix}_{hash}_{timestamp}@{domain}".format(
        **{
            "prefix": "temp",
            "hash": "".join(random.choice(allowed_chars) for _ in range(length)),
            "timestamp": int(datetime.now().timestamp()),
            "domain": settings.EMAIL_DEFAULT_DOMAIN,
        }
    )


def get_real_ip(request: Request) -> Optional[str]:
    return request.headers.get("x-forwarded-for")


async def get_country_by_ip(session: AsyncSession, ip_address: str) -> Optional[Country]:
    try:
        with geoip2.database.Reader(geo_ip_db_path) as reader:
            result = reader.country(ip_address=ip_address)

            ip_country = await crud.country.get_by_iso_code(db=session, iso_code=result.country.iso_code)

            return ip_country
    except (AddressNotFoundError, FileNotFoundError):
        return None
