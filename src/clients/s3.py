import io

from aioboto3 import Session

from core.config import settings


class S3Client:
    def __init__(self):
        self.session = Session()

    async def download_file(self, file_path: str) -> bytes:
        async with self.session.client(
            "s3",
            endpoint_url=settings.AWS_S3_ENDPOINT_URL,
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
        ) as s3:
            s3_ob = await s3.get_object(
                Bucket=settings.AWS_S3_BUCKET_NAME,
                Key=file_path,
            )

            async with s3_ob["Body"] as stream:
                return await stream.read()

    async def remove_file(self, file_path: str) -> None:
        async with self.session.client(
            "s3",
            endpoint_url=settings.AWS_S3_ENDPOINT_URL,
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
        ) as s3:
            await s3.delete_object(
                Bucket=settings.AWS_S3_BUCKET_NAME,
                Key=file_path,
            )

    async def upload_file(self, file_path: str, file_obj: io.BytesIO) -> None:
        async with self.session.client(
            "s3",
            endpoint_url=settings.AWS_S3_ENDPOINT_URL,
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
        ) as s3:
            await s3.upload_fileobj(
                file_obj,
                settings.AWS_S3_BUCKET_NAME,
                file_path,
            )


s3_client = S3Client()
