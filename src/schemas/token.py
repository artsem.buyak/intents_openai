from datetime import datetime
from typing import Optional

from pydantic import BaseModel


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenPayload(BaseModel):
    sub: Optional[int] = None


class UserTokenPayload(Token):
    user_id: int
    email: str
    created: bool
    created_at: datetime
