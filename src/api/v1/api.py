from fastapi import APIRouter

from api.v1.endpoints import admin, intents, login, social_auth, users

api_router = APIRouter()

api_router.include_router(login.router, tags=["login"])
api_router.include_router(users.router, tags=["users"])
api_router.include_router(intents.router, tags=["completion"])
api_router.include_router(social_auth.router, prefix="/social", tags=["social"])
api_router.include_router(admin.router, prefix="/admin", tags=["admin"])
